public class SavingsAccount extends BankAccount {
    private int safetyDepositBoxID;
    private int safetyDepositBoxAccessCode;


    public SavingsAccount(String name, String ssn, double initialDeposit) {
        super(name, ssn, initialDeposit);
    }

    public void showInfo() {
        System.out.println("****Savings Account****");
        super.showInfo();
        System.out.println("safetyDepositBoxID : "+safetyDepositBoxID+"" +
                "\nsafetyDepositBoxAccessCode : "+safetyDepositBoxAccessCode);
    }

    public void assignSafetyDepositBox(int safetyDepositBoxID, int safetyDepositBoxAccessCode) {
        this.safetyDepositBoxID=safetyDepositBoxID;
        this.safetyDepositBoxAccessCode=safetyDepositBoxAccessCode;
    }
    public static double computeBaseInterestRate(){
        return baseRate-0.25;
    }
}