import java.io.*;
import java.util.HashSet;
import java.util.Iterator;

public class ProcessCustomerAccountCreationRequest {
    private static final HashSet<BankAccount> bankAccountHashSet=new HashSet<>();

    public static void main(String[] args) throws IOException {
        String customerDetails;
        BufferedReader bufferedReader = new BufferedReader
                (new FileReader("src/main/resources/NewBankAccounts.csv"));
        BankAccountFactory bankAccountFactory = new BankAccountFactory();
        while ((customerDetails = bufferedReader.readLine())!=null){
            // process each customer one by one here.
            String[] customerDetailSplit = customerDetails.split(",");
            BankAccount bankAccount = bankAccountFactory.createAccount
                    (customerDetailSplit[0], customerDetailSplit[1], customerDetailSplit[2], Double.parseDouble(customerDetailSplit[3]));
            bankAccountHashSet.add(bankAccount);
            bankAccount.showInfo();
        }
        System.out.println("-------------------------------------------------------");

        System.out.println("SavingsAccount.computeBaseInterestRate :: "+SavingsAccount.computeBaseInterestRate());
        System.out.println("CheckingAccount.computeBaseInterestRate :: "+CheckingAccount.computeBaseInterestRate());

        System.out.println("-------------------------------------------------------");
        Iterator<BankAccount> acIterator=bankAccountHashSet.iterator();
        BankAccount account= acIterator.next();
        account.deposit(100);
        account.withdraw(200);
        account.transfer(3000.0, acIterator.next());
    }

}
