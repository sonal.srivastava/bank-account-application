import org.apache.commons.lang3.RandomStringUtils;

public class BankAccountFactory {

    private static int uniqueFiveDigitNumber = 10000;
    private static int safetyDepositBoxID=100;

    public BankAccount createAccount(String name, String ssn, String accountType, double initialDeposit) {
        if (accountType.equalsIgnoreCase("Savings")) {
            SavingsAccount savingsAccount = new SavingsAccount(name, ssn, initialDeposit);
            savingsAccount.setAccountNumber(generateAccountNumber("1",ssn));
            savingsAccount.assignSafetyDepositBox(safetyDepositBoxID++,
                    Integer.parseInt(RandomStringUtils.randomNumeric(4)));

            return savingsAccount;
        }
        else {
            CheckingAccount checkingAccount = new CheckingAccount(name, ssn, initialDeposit);
            checkingAccount.setAccountNumber(generateAccountNumber("2",ssn));
            checkingAccount.assignDebitCard(RandomStringUtils.randomNumeric(12),
                    Integer.parseInt(RandomStringUtils.randomNumeric(4)));
            return checkingAccount;
        }
    }

    private String generateAccountNumber(String acType, String ssn) {
        return acType+ssn.substring(7,9)+uniqueFiveDigitNumber++ + RandomStringUtils.randomNumeric(3);
    }
}
