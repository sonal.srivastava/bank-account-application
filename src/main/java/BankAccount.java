public class BankAccount {
    private final String name;
    private final String ssn;
    private String accountNumber;
    private double balance;
    protected static double baseRate=600;

    public BankAccount(String name, String ssn, double initialDeposit) {
        this.name = name;
        this.ssn = ssn;
        this.balance = initialDeposit;
    }

    public void deposit(double amount) {
        balance+=amount;
        showBalance();
    }

    private void showBalance() {
        System.out.println("Balance is : "+balance);
    }

    public void withdraw(double amount) {
        balance-=amount;
        showBalance();
    }

    public void transfer(double amount, BankAccount otherBankAccount) {
        withdraw(amount);
        otherBankAccount.deposit(amount);
    }

    public void showInfo() {
        System.out.println("Name : "+name+" / "+ssn+"\nAccount Number : "+accountNumber+"\n Balance : "+balance);
    }

    public void setAccountNumber(String generateAccountNumber) {
        accountNumber=generateAccountNumber;
        System.out.println("Account Number generated is : "+ accountNumber);
    }

}
