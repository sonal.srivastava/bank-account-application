public class CheckingAccount extends BankAccount{
    private String debitCardNumber;
    private int debitCardPIN;

    public CheckingAccount(String name, String ssn, double initialDeposit) {
        super(name, ssn, initialDeposit);
    }


    @Override
    public void showInfo() {
        System.out.println("**** Checking Account ****");
        super.showInfo();
        System.out.println("debitCardNumber : "+debitCardNumber+"\ndebitCardPIN : "+debitCardPIN);
    }

    public void assignDebitCard(String debitCardNumber, int debitCardPIN) {
        this.debitCardNumber=debitCardNumber;
        this.debitCardPIN=debitCardPIN;
    }
    public static double computeBaseInterestRate(){
        return baseRate*0.15;
    }
}
